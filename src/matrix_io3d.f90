module matrix_io3d
    use matio

    implicit none

contains

    subroutine mio3d_createFlag(nX, nY, nZ, flagMatrix)
        implicit none

        integer, intent(in):: nX, nY, nZ

        integer, dimension(nX, nY, nZ), intent(out) :: flagMatrix

        integer :: z1, z2, y1, y2, x1, x2, z_head

        flagMatrix = 0
        z_head = 0.1 * nZ

        if (nX < 10 .or. nY < 10 .or. nZ < 10) then
            flagMatrix(1, 1, 1) = 1
            flagMatrix(nX, nY, nZ) = 1
        else
			! boundaries
            flagMatrix(1:nX, 1:nY, nZ) = 1
            flagMatrix(1:nX, 1:nY, 1) = 1

            ! flat head
            z1 = nZ * 0.1
            z1 = z_head
            flagMatrix(1:nX, 1:nY, 1:z1) = 1

            ! probe pin
			z1 = 1
			z2 = nZ * 0.25
			y1 = 0.45 * nY
			y2 = 0.55 * nY
			x1 = 0.45 * nX
			x2 = 0.55 * nX
!            flagMatrix(x1:x2, y1:y2, z1:z2) = 1

        endif

    end subroutine mio3d_createFlag

    subroutine mio3d_createPlasma(nX, nY, nZ, flagMatrix, potentialMatrix, densityMatrix)
        use IFPORT
        implicit none

        integer, intent(in):: nX, nY, nZ
        integer, dimension(nX, nY, nZ), intent(in) :: flagMatrix
        real*8, dimension(nX, nY, nZ), intent(out) :: potentialMatrix, densityMatrix

        integer :: z1, z2, y1, y2, x1, x2, x, y, z
        real *8  :: z_head, l_sh, l_dec

        densityMatrix = 0
        call srand(1234)

        z_head = 0.1 * nZ

        if (nX < 10 .or. nY < 10 .or. nZ < 10) then
            potentialMatrix(1, 1, 1) = 3
            potentialMatrix(nX, nY, nZ) = -3
        else
			! boundaries
            potentialMatrix(1:nX, 1:nY, nZ) = 0
            potentialMatrix(1:nX, 1:nY, 1) = -3

            ! flat head
            z1 = nZ * 0.1
            z1 = z_head
            potentialMatrix(1:nX, 1:nY, 1:z1) = -3

            ! probe pin
			z1 = 1
			z2 = nZ * 0.25
			y1 = 0.45 * nY
			y2 = 0.55 * nY
			x1 = 0.45 * nX
			x2 = 0.55 * nX
!            potentialMatrix(x1:x2, y1:y2, z1:z2) = -10

        endif

        l_sh = 2;
        l_dec = 2;
        if (nZ < 64) then
            l_dec = l_sh
        endif
        do x = 1, nX
            do y = 1, nY
                do z = 1, nZ
                    if (flagMatrix(x, y, z) == 0) then
                        if (z .le. z_head + l_sh) then
                            densityMatrix(x, y, z) = 50 * (0.5 * dexp(-l_sh/l_dec))  !+ 50 * (0.5 * (rand() - 0.5))
                        else
                            densityMatrix(x, y, z) = 50 * (0.5 * dexp((-z + z_head) / l_dec))  !+ 50 * (0.5 * (rand() - 0.5))
                        endif
                    endif
                enddo
            enddo
        enddo

    end subroutine mio3d_createPlasma

    subroutine mio3d_loadDimensions(inputFile, nX, nY, nZ, dX, dY, dZ, n0)
        implicit none

        character*2048, intent(in):: inputFile

        integer, intent(out) :: nX, nY, nZ
        real*8, intent(out) :: dX, dY, dZ
        integer, intent(out) :: n0

        integer:: use_compression, ierr, version
        type(MAT_T):: MAT
        type(MATVAR_T):: MATVAR

        write (*,*) '% Loading dimensions from file '//trim(inputFile)

        call fmat_loginit('loadArraySize')
        ierr = fmat_open(trim(inputFile)//char(0), mat_acc_rdonly, MAT)
        write (*,*) '#	Open ok', ierr

        ierr = fmat_varreadinfo(MAT, 'Nx'//char(0), MATVAR)
        ierr = fmat_varreaddata(MAT, MATVAR, nX)
        write (*,*) '#	Nx ok', nX

        ierr = fmat_varreadinfo(MAT, 'Ny'//char(0), MATVAR)
        ierr = fmat_varreaddata(MAT, MATVAR, nY)
        write (*,*) '#	Ny ok', nY

        ierr = fmat_varreadinfo(MAT, 'Nz'//char(0), MATVAR)
        ierr = fmat_varreaddata(MAT, MATVAR, nZ)
        write (*,*) '#	Nz ok', nZ

        dZ = 1.0
        dY = dZ
        dX = dZ


        !ierr = fmat_varreadinfo(MAT, 'N0'//char(0), MATVAR)
        !ierr = fmat_varreaddata(MAT, MATVAR, n0)

        ! npc
        n0 = 50 / (1 * dY * dZ);
        ierr = fmat_close(mat)

    end subroutine mio3d_loadDimensions

    subroutine mio3d_loadFlag(inputFile, nX, nY, nZ, flagMatrix)
        implicit none

        character*2048, intent(in):: inputFile

        integer, intent(in):: nX, nY, nZ

        integer, dimension(nX, nY, nZ), intent(out) :: flagMatrix

        integer:: use_compression, ierr, version
        type(MAT_T):: MAT
        type(MATVAR_T):: MATVAR
        integer :: x, y, z

        write (*,*) '#	Loading flagMatrix from file '//trim(inputFile)

        flagMatrix = 1

        call fmat_loginit('loadArrays')
        ierr = fmat_open(trim(inputFile)//char(0), mat_acc_rdonly, MAT)

        ierr = fmat_varreadinfo(MAT, 'objects'//char(0), MATVAR)
        ierr = fmat_varreaddata(MAT, MATVAR, flagMatrix)


        !    do x = 1, nX
        !        do y = 1, nY
        !            do z = 1, nZ
        !                if (flagMatrix(x, y, z) > 0) then
        !                    flagMatrix(x, y, z) = 0
        !                else
        !                    flagMatrix(x, y, z) = 1
        !                endif
        !            enddo
        !        enddo
        !    enddo
        ierr = fmat_close(mat)


    end subroutine mio3d_loadFlag

    subroutine mio3d_loadPlasma(inputFile, nX, nY, nZ, potentialMatrix, densityMatrix)
        implicit none

        character*2048, intent(in):: inputFile

        integer, intent(in):: nX, nY, nZ

        real*8, dimension(nX, nY, nZ), intent(out) :: potentialMatrix, densityMatrix

        integer:: use_compression, ierr, version
        type(MAT_T):: MAT
        type(MATVAR_T):: MATVAR

        write (*,*) '#	Loading input file '//trim(inputFile)

        call fmat_loginit('loadPlasma')
        ierr = fmat_open(trim(inputFile)//char(0), mat_acc_rdonly, MAT)

        ierr = fmat_varreadinfo(MAT, 'Pot'//char(0), MATVAR)
        ierr = fmat_varreaddata(MAT, MATVAR, potentialMatrix)

        ierr = fmat_varreadinfo(MAT, 'rho'//char(0), MATVAR)
        ierr = fmat_varreaddata(MAT, MATVAR, densityMatrix)
        ierr = fmat_close(mat)

    end subroutine mio3d_loadPlasma

    subroutine mio3d_saveResults(outputFile, nX, nY, nZ, potentialMatrix, densityMatrix, flagMatrix, steps, stats, init_time, n_parts, grid_partitioning, n_procs, mem_usage, vm_size)
        implicit none

        character*2048, intent(in):: outputFile

        integer, intent(in):: nX, nY, nZ, steps

        real*8, dimension(nX, nY, nZ), intent(in) :: potentialMatrix, densityMatrix
        integer, dimension(nX, nY, nZ), intent(in) :: flagMatrix
		real*8, dimension(steps), intent(in) :: stats
		real*8, intent(in) :: init_time
		integer, intent(in) :: n_parts
		integer, dimension(n_parts, 6), intent(in) :: grid_partitioning
		integer, intent(in) :: n_procs
		integer*8, dimension(steps + 1, n_procs), intent(in) :: mem_usage, vm_size
		real*8, dimension(steps + 1, n_procs) :: mem_usage_r, vm_size_r

        integer:: use_compression, ierr, version
        type(MAT_T):: MAT
        type(MATVAR_T):: MATVAR

        use_compression = 0
        mem_usage_r = dble(mem_usage)
        vm_size_r = dble(vm_size)

        write (*,*) '#	Saving output file '//trim(outputFile)

        CALL fmat_loginit('savePlasma')

        ierr = fmat_create(trim(outputFile)//char(0), mat)

        ierr = fmat_varcreate('potentialMatrix', potentialMatrix, matvar)
        ierr = fmat_varwrite(mat, matvar, potentialMatrix, use_compression)
        ierr = fmat_varfree(matvar)

        ierr = fmat_varcreate('densityMatrix', densityMatrix, matvar)
        ierr = fmat_varwrite(mat, matvar, densityMatrix, use_compression)
        ierr = fmat_varfree(matvar)

        ierr = fmat_varcreate('flagMatrix', flagMatrix, matvar)
        ierr = fmat_varwrite(mat, matvar, flagMatrix, use_compression)
        ierr = fmat_varfree(matvar)

        ierr = fmat_varcreate('stats', stats, matvar)
        ierr = fmat_varwrite(mat, matvar, stats, use_compression)
        ierr = fmat_varfree(matvar)

        ierr = fmat_varcreate('init_time', init_time, matvar)
        ierr = fmat_varwrite(mat, matvar, init_time, use_compression)
        ierr = fmat_varfree(matvar)

        ierr = fmat_varcreate('grid_partitioning', grid_partitioning, matvar)
        ierr = fmat_varwrite(mat, matvar, grid_partitioning, use_compression)
        ierr = fmat_varfree(matvar)

        ierr = fmat_varcreate('mem_usage', mem_usage_r, matvar)
        ierr = fmat_varwrite(mat, matvar, mem_usage_r, use_compression)
        ierr = fmat_varfree(matvar)

        ierr = fmat_varcreate('vm_size', vm_size_r, matvar)
        ierr = fmat_varwrite(mat, matvar, vm_size_r, use_compression)
        ierr = fmat_varfree(matvar)

        ierr = fmat_close(mat)
        write (6,*) '#	Plasma written.'

    end subroutine mio3d_saveResults

    subroutine mio3d_saveMatrix(outputFile, elementCount, matrixDimension, Ai, Aj, Ax)
        implicit none

        character*2048, intent(in):: outputFile

        integer, intent(in) :: elementCount, matrixDimension
        integer, dimension(elementCount), intent(in) :: Ai, Aj
        real*8, dimension(elementCount), intent(in) :: Ax

        integer:: use_compression, ierr, version
        type(MAT_T):: MAT
        type(MATVAR_T):: MATVAR

        use_compression = 0

        write (*,*) '#	Saving output file '//trim(outputFile)
        write (*,*) '#	init'

        CALL fmat_loginit('saveMatrix')
        write (*,*) '#	init ok', elementCount, matrixDimension

        ierr = fmat_create(trim(outputFile)//char(0), mat)

        ierr = fmat_varcreate('elementCount', elementCount, matvar)
        ierr = fmat_varwrite(mat, matvar, elementCount, use_compression)
        ierr = fmat_varfree(matvar)
        write (*,*) '#	elementCount', ierr

        ierr = fmat_varcreate('matrixDimension', matrixDimension, matvar)
        ierr = fmat_varwrite(mat, matvar, matrixDimension, use_compression)
        ierr = fmat_varfree(matvar)
        write (*,*) '#	matrixDimension', ierr

        ierr = fmat_varcreate('Ai', Ai, matvar)
        ierr = fmat_varwrite(mat, matvar, Ai, use_compression)
        ierr = fmat_varfree(matvar)
        write (*,*) '#	Ai', ierr

        ierr = fmat_varcreate('Aj', Aj, matvar)
        ierr = fmat_varwrite(mat, matvar, Aj, use_compression)
        ierr = fmat_varfree(matvar)
        write (*,*) '#	Aj', ierr

        ierr = fmat_varcreate('Ax', Ax, matvar)
        ierr = fmat_varwrite(mat, matvar, Ax, use_compression)
        ierr = fmat_varfree(matvar)
        write (*,*) '#	Ax', ierr

        ierr = fmat_close(mat)
        write (6,*) '#	Matrix written.'

    end subroutine mio3d_saveMatrix


    subroutine mio3d_saveMatrixCsr(outputFile, elementCount, matrixDimension, Bi, Bp, Bx)
        implicit none

        character*2048, intent(in):: outputFile

        integer, intent(in) :: elementCount, matrixDimension
        integer, dimension(elementCount), intent(in) :: Bi
        integer, dimension(matrixDimension + 1), intent(in) :: Bp
        real*8, dimension(elementCount), intent(in) :: Bx

        integer:: use_compression, ierr, version
        type(MAT_T):: MAT
        type(MATVAR_T):: MATVAR

        use_compression = 0

        write (*,*) '#	Saving output file '//trim(outputFile)

        CALL fmat_loginit('saveMatrix')

        ierr = fmat_create(trim(outputFile)//char(0), mat)

        ierr = fmat_varcreate('elementCount', elementCount, matvar)
        ierr = fmat_varwrite(mat, matvar, elementCount, use_compression)
        ierr = fmat_varfree(matvar)


        ierr = fmat_varcreate('matrixDimension', matrixDimension, matvar)
        ierr = fmat_varwrite(mat, matvar, matrixDimension, use_compression)
        ierr = fmat_varfree(matvar)


        ierr = fmat_varcreate('Bi', Bi, matvar)
        ierr = fmat_varwrite(mat, matvar, Bi, use_compression)
        ierr = fmat_varfree(matvar)

        ierr = fmat_varcreate('Bp', Bp, matvar)
        ierr = fmat_varwrite(mat, matvar, Bp, use_compression)
        ierr = fmat_varfree(matvar)

        ierr = fmat_varcreate('Bx', Bx, matvar)
        ierr = fmat_varwrite(mat, matvar, Bx, use_compression)
        ierr = fmat_varfree(matvar)

        ierr = fmat_close(mat)
        write (6,*) '#	CSR matrix written.'

    end subroutine mio3d_saveMatrixCsr

end module matrix_io3d

