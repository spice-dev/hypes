
export OMP_NUM_THREADS=1

rm *.log

MPI_PROC=16

echo "Test for n=$DIM and n_cpu=$MPI_PROC is starting"
TEST_NAME=s3fail
WORK_DIR=logs-debug/$TEST_NAME/$MPI_PROC
mkdir -p $WORK_DIR
mpirun -np $MPI_PROC -outfile-pattern=$WORK_DIR/$TEST_NAME.%r.log ../bin/autotest.bin -o $WORK_DIR/$TEST_NAME-o.mat -x 273 -y 417 -z 113 -a 4 -b 4

echo "Test for n=$DIM and n_cpu=$MPI_PROC done"

rm -f core.*
