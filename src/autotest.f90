program pes3dtest
    use getoptions
    use mpi
    use matrix_io3d
    use pes3d
    use IFPORT

	implicit none

    character :: okey
    character*2048 :: inputFile, outputFile
    logical :: use_input

    type (solver_info) :: pes_info

    integer :: nX, nY, nZ
    real*8 :: dX, dY, dZ
    integer :: n0
    integer, dimension(:,:,:), allocatable :: flagMatrix
    real*8, dimension(:,:,:), allocatable :: potentialMatrix, potentialMatrix2, densityMatrix, densityMatrix2, pot_mask_m_glob

    integer, dimension(:,:,:), allocatable :: flag_m_loc
    real*8, dimension(:,:,:), allocatable :: pot_m_loc, dens_m_loc, newpot_m_loc, pot_mask_m_loc, rand_matrix

    integer :: mympi_commsize, mympi_rank, mympi_ierr, mympi_provided_threading, mympi_tag
    integer :: mympi_status(MPI_STATUS_SIZE)


    integer :: elementCount, matrix_dimension
    integer*8 :: time1, time2, time_diff
    real*8 :: init_time

    integer, parameter :: steps = 10
	real*8, dimension(steps) :: stats

    integer :: i, i_x, i_y, i_z, i_r
    real*8 :: one

    integer :: p_x, p_y, p_z, s_x, s_y, s_z, x_from_loc, x_to_loc, y_from_loc, y_to_loc, z_from_loc, z_to_loc, x_to, x_from, y_to, y_from, z_to, z_from, n_loc, n_x_loc, n_y_loc, n_z_loc
    integer, dimension(:,:), allocatable :: partitioning

    real*8, dimension(:,:,:), allocatable :: dum_r
    integer, dimension(:,:,:), allocatable :: dum_i
    integer, dimension(5,6,8) :: dum_ix

    integer*8, dimension(:,:), allocatable :: mem_usage, vm_size
    integer*8, dimension(:), allocatable :: mem_usage_step, vm_size_step

    real*8 :: n_0

    call srand(1234)
    call RANDOM_SEED()

    n_0 = 50
    p_x = 1
    p_y = 1
    p_z = 1

    use_input = .false.

    do
        okey=getopt('i:o:x:y:z:a:b:mn')

		if(okey == 'i') then
			inputFile = trim(optarg)
			write (*,*) 'Using input file ', trim(inputFile)
			use_input = .true.
		end if

        if(okey.eq.'o') then
            outputFile=trim(optarg)
            write(*,*) 'The output will be: ', trim(outputFile)
        end if

        if(okey.eq.'x') then
            read (optarg,*) nX
            write(*,*) 'Size in x: ', nX
        end if

        if(okey.eq.'y') then
            read (optarg,*) nY
            write(*,*) 'Size in y: ', nY
        end if

        if(okey.eq.'z') then
            read (optarg,*) nZ
            write(*,*) 'Size in z: ', nZ
        end if

        if(okey.eq.'a') then
            read (optarg,*) p_x
            write(*,*) 'Partitioning in x: ', p_x
        end if

        if(okey.eq.'b') then
            read (optarg,*) p_y
            write(*,*) 'Partitioning in y: ', p_y
        end if

        if(okey.eq.'>') exit

    enddo



	call MPI_INIT_THREAD(MPI_THREAD_MULTIPLE, mympi_provided_threading, mympi_ierr)
    write(*,*) 'MPI Threaded init', MPI_THREAD_MULTIPLE, mympi_provided_threading

	call MPI_COMM_SIZE(MPI_COMM_WORLD, mympi_commsize, mympi_ierr)
	call MPI_COMM_RANK(MPI_COMM_WORLD, mympi_rank, mympi_ierr)


	if (mympi_rank == 0 .and. use_input) then
		call mio3d_loadDimensions(inputFile, nX, nY, nZ, dX, dY, dZ, n0)
		write (*,*) 'Loaded dimensions', nX, nY, nZ
		write (*,*) 'Loaded grid and plasma', dY, dZ, n0
		!dZ = 0.5
		!dY = 0.5
	endif


	allocate(mem_usage(steps + 1, mympi_commsize))
	allocate(mem_usage_step(mympi_commsize))
	allocate(vm_size(steps + 1, mympi_commsize))
	allocate(vm_size_step(mympi_commsize))
	mem_usage = 0
	mem_usage_step = 0
	vm_size = 0
	vm_size_step = 0


    if (mympi_rank == 0) then
        dX = 1
        dY = 1
        dZ = 1
        n0 = 50
    endif

    call MPI_BCAST(nX, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(nY, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(nZ, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(dX, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(dY, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(dZ, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(n0, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, mympi_ierr)

    write (*,*) 'Dimensions loaded and distributed.', nX, nY, nZ, dX, dY, dZ, n0
    write (*,*) 'mympi_commsize', mympi_commsize
    allocate(partitioning(mympi_commsize, 6))

    p_z = mympi_commsize / (p_x * p_y)
    s_x =  nX / p_x;
    s_y =  nY / p_y;
    s_z =  nZ / p_z;

    write (*,*) 'partitioning.', p_x, p_y, p_z
    i_r = 1
    do i_x = 1, p_x
        do i_y = 1, p_y
            do i_z = 1, p_z
                x_from = 1 + (i_x - 1) * s_x
                x_to = i_x * s_x
                if (i_x .eq. p_x) then
                    x_to = Nx
                endif

                y_from = 1 + (i_y - 1) * s_y
                y_to = i_y * s_y
                if (i_y .eq. p_y) then
                    y_to = Ny
                endif

                z_from = 1 + (i_z - 1) * s_z
                z_to = i_z * s_z
                if (i_z .eq. p_z) then
                    z_to = Nz
                endif

                if (i_r .eq. mympi_rank + 1) then
                    x_from_loc = x_from
                    y_from_loc = y_from
                    z_from_loc = z_from

                    x_to_loc = x_to
                    y_to_loc = y_to
                    z_to_loc = z_to

                    n_x_loc = x_to_loc - x_from_loc + 1
                    n_y_loc = y_to_loc - y_from_loc + 1
                    n_z_loc = z_to_loc - z_from_loc + 1
                endif

                partitioning(i_r, 1) = x_from
                partitioning(i_r, 2) = x_to
                partitioning(i_r, 3) = y_from
                partitioning(i_r, 4) = y_to
                partitioning(i_r, 5) = z_from
                partitioning(i_r, 6) = z_to

                i_r = i_r + 1
            enddo
        enddo
    enddo

    write (*,*) 'Partitioning defined.'
    write (*,*) 'x_from x_to', x_from_loc, x_to_loc
    write (*,*) 'y_from y_to', y_from_loc, y_to_loc
    write (*,*) 'z_from z_to', z_from_loc, z_to_loc


    matrix_dimension = nX * nY * nZ

    if (mympi_rank == 0) then
        allocate(flagMatrix(nX, nY, nZ))
        allocate(potentialMatrix(nX, nY, nZ))
        allocate(potentialMatrix2(nX, nY, nZ))
        allocate(densityMatrix(nX, nY, nZ))
        allocate(densityMatrix2(nX, nZ, nY))

        write (*,*) 'Creating dummy flag matrices'
        if (use_input) then
            call mio3d_loadFlag(inputFile, nX, nY, nZ, flagMatrix)
            call mio3d_loadPlasma(inputFile, nX, nY, nZ, potentialMatrix, densityMatrix)
        else
            call mio3d_createFlag(nX, nY, nZ, flagMatrix)
            call mio3d_createPlasma(nX, nY, nZ, flagMatrix, potentialMatrix, densityMatrix)
        endif

    endif

    allocate(flag_m_loc(n_x_loc,n_y_loc,n_z_loc))
    allocate(pot_m_loc(n_x_loc,n_y_loc,n_z_loc))
    allocate(newpot_m_loc(n_x_loc,n_y_loc,n_z_loc))
    allocate(dens_m_loc(n_x_loc,n_y_loc,n_z_loc))

    allocate(rand_matrix(n_x_loc,n_y_loc,n_z_loc))

    dens_m_loc = 0

    write (*,*) 'Syncing threads'
    call MPI_BARRIER(MPI_COMM_WORLD, mympi_ierr)

!    i_r = 0
!    mympi_tag = 990000
!    do i_r = 0, mympi_commsize - 1
!        write(*,*), i_r
!        x_from = partitioning(i_r + 1, 1)
!        x_to = partitioning(i_r + 1, 2)
!        y_from = partitioning(i_r + 1, 3)
!        y_to = partitioning(i_r + 1, 4)
!        z_from = partitioning(i_r + 1, 5)
!        z_to = partitioning(i_r + 1, 6)
!
!        write(*,*) 'Processing ', i_r
!
!        if (i_r .eq. 0) then
!            if (mympi_rank .eq. 0) then
!                flag_m_loc = flagMatrix(x_from:x_to,y_from:y_to,z_from:z_to)
!                pot_m_loc = potentialMatrix(x_from:x_to,y_from:y_to,z_from:z_to)
!                dens_m_loc = densityMatrix(x_from:x_to,y_from:y_to,z_from:z_to)
!                write(*,*) 'Data copied'
!
!            endif
!        else
!            n_loc = (x_to-x_from+1)*(y_to-y_from+1)*(z_to-z_from+1)
!            write(*,*) 'n_loc', n_loc
!            if (mympi_rank .eq. 0) then
!                call mpi_send(flagMatrix(x_from:x_to,y_from:y_to,z_from:z_to), n_loc, MPI_INT, i_r, 991000 + i_r, MPI_COMM_WORLD, mympi_ierr)
!                call mpi_send(potentialMatrix(x_from:x_to,y_from:y_to,z_from:z_to), n_loc, MPI_DOUBLE, i_r, 992000 + i_r, MPI_COMM_WORLD, mympi_ierr)
!                call mpi_send(densityMatrix(x_from:x_to,y_from:y_to,z_from:z_to), n_loc, MPI_DOUBLE, i_r, 993000 + i_r, MPI_COMM_WORLD, mympi_ierr)
!                write(*,*) 'Data sent to ', i_r
!            elseif (mympi_rank .eq. i_r) then
!                call mpi_recv(flag_m_loc, n_loc, MPI_INT, 0, 991000 + i_r, MPI_COMM_WORLD, mympi_status, mympi_ierr)
!                call mpi_recv(pot_m_loc, n_loc, MPI_DOUBLE, 0, 992000 + i_r, MPI_COMM_WORLD, mympi_status, mympi_ierr)
!                call mpi_recv(dens_m_loc, n_loc, MPI_DOUBLE, 0, 993000 + i_r, MPI_COMM_WORLD, mympi_status, mympi_ierr)
!                write(*,*) 'Data recieved on ', i_r
!            else
!                write(*,*) 'Not sent'
!            endif
!        endif
!
!    enddo

    call pes3d_set_mpi(pes_info, MPI_COMM_WORLD, mympi_commsize, partitioning)
    call pes3d_scatter_i(pes_info, nX, nY, nZ, flagMatrix, n_x_loc, n_y_loc, n_z_loc, flag_m_loc)
    write (*,*) flag_m_loc
    call pes3d_scatter_r(pes_info, nX, nY, nZ, potentialMatrix, n_x_loc, n_y_loc, n_z_loc, pot_m_loc)
    call pes3d_scatter_r(pes_info, nX, nY, nZ, densityMatrix, n_x_loc, n_y_loc, n_z_loc, dens_m_loc)

    call gettime(time1)
    !call pes3d_init(mumpsPar, MPI_COMM_WORLD, nX, nY, nZ, dX, dY, dZ, flagMatrix)
    !call pes3d_init(pes_info, MPI_COMM_WORLD, nX, nY, nZ, dX, dY, dZ, flagMatrix)
    call pes3d_init(pes_info, MPI_COMM_WORLD, .true., nX, nY, nZ, dX, dY, dZ, n_0, n_x_loc, n_y_loc, n_z_loc, x_from_loc, y_from_loc, z_from_loc, flag_m_loc)
    call gettime(time2)
    time_diff = time2 - time1
    init_time = 1.0 * time_diff
    write (*,*) '#time_diff;init;', time_diff
    call get_cluster_memory_usage_kb(mem_usage_step, vm_size_step, mympi_commsize)
    mem_usage(1,:) = mem_usage_step(:)
    vm_size(1,:) = vm_size_step(:)

    one = 1
    do i = 1, steps
        !dens_m_loc = dens_m_loc + (0.01 * one * (i - 1))
        !dens_m_loc = 0!dens_m_loc + (0.01 * rand() * (mod(i, 3)-1))
        call gettime(time1)
        call RANDOM_NUMBER(rand_matrix)
        dens_m_loc = dens_m_loc + 50 * (0.0005 * (rand_matrix - 0.5))
        call gettime(time2)
        time_diff = time2 - time1
        write (*,*) '#time_diff;dens;', time_diff



        write (*,*) 'Density sum', sum(dens_m_loc)

        one = - one
        call gettime(time1)
!        int get_cluster_memory_usage_kb_(long* vmrss_per_process, long* vmsize_per_process, int root, int np)

        !call pes3d_solve(mumpsPar, nX, nY, nZ, dX, dY, dZ, flagMatrix, potentialMatrix, densityMatrix2, potentialMatrix2)
        call pes3d_solve(pes_info, n_x_loc, n_y_loc, n_z_loc, flag_m_loc, pot_m_loc, dens_m_loc, newpot_m_loc)
        call pes3d_gather_r(pes_info, nX, nY, nZ, potentialMatrix2, n_x_loc, n_y_loc, n_z_loc, newpot_m_loc)

        call gettime(time2)
        time_diff = time2 - time1
        write (*,*) '#time_diff;solve;', time_diff
        stats(i) = 1.0 * time_diff

        call get_cluster_memory_usage_kb(mem_usage_step, vm_size_step, mympi_commsize)
        mem_usage(i + 1,:) = mem_usage_step(:)
        vm_size(i + 1,:) = vm_size_step(:)

    enddo

!    do i_r = 0, mympi_commsize - 1
!        x_from = partitioning(i_r + 1, 1)
!        x_to = partitioning(i_r + 1, 2)
!        y_from = partitioning(i_r + 1, 3)
!        y_to = partitioning(i_r + 1, 4)
!        z_from = partitioning(i_r + 1, 5)
!        z_to = partitioning(i_r + 1, 6)
!
!
!        if (i_r .eq. 0) then
!            if (mympi_rank .eq. 0) then
!                potentialMatrix2(x_from:x_to,y_from:y_to,z_from:z_to) = newpot_m_loc
!                write(*,*) 'data copied'
!            endif
!        else
!            n_loc = (x_to-x_from+1)*(y_to-y_from+1)*(z_to-z_from+1)
!            write(*,*) 'i_r, n_loc', i_r, n_loc
!            if (mympi_rank .eq. 0) then
!
!                write(*,*) 'Preparing reception', i_r, 'bnds', x_from, x_to, y_from, y_to, z_from, z_to
!                write(*,*) 'n_loc', n_loc
!                call mpi_recv(potentialMatrix2(x_from:x_to,y_from:y_to,z_from:z_to), n_loc, MPI_DOUBLE, i_r, 1994000 + i_r, MPI_COMM_WORLD, mympi_status, mympi_ierr)
!                write(*,*) 'potentialMatrix2 recieved ', i_r
!
!            elseif (mympi_rank .eq. i_r) then
!                write(*,*) 'Preparing send', i_r, 'bnds', x_from, x_to, y_from, y_to, z_from, z_to
!
!                write(*,*) '----'
!                !write(*,*) flag_m_loc
!                write(*,*) '----'
!
!                call mpi_send(newpot_m_loc, n_loc, MPI_DOUBLE, 0, 1994000 + i_r, MPI_COMM_WORLD, mympi_ierr)
!                write(*,*) 'newpot_m_loc sent to', 0
!            else
!                write(*,*) 'Not sent'
!            endif
!        endif
!
!    enddo

    call MPI_BARRIER(MPI_COMM_WORLD, mympi_ierr)

    if (mympi_rank == 0) then
        !write(*,*) potentialMatrix

        call mio3d_saveResults(outputFile, nX, nY, nZ, potentialMatrix2, densityMatrix, flagMatrix, steps, stats, init_time, mympi_commsize, partitioning, mympi_commsize, mem_usage, vm_size)
    endif



    call pes3d_stop(pes_info)
    write(*,*) 'Stopped 1 '

    if (mympi_rank == 0) then
        deallocate(flagMatrix)
        deallocate(potentialMatrix)
        deallocate(potentialMatrix2)
        deallocate(densityMatrix)
        deallocate(densityMatrix2)
    endif

    write(*,*) 'Deallocated 1 '


    deallocate(flag_m_loc)
    deallocate(pot_m_loc)
    deallocate(newpot_m_loc)
    deallocate(dens_m_loc)
    deallocate(rand_matrix)

    deallocate(mem_usage)
    deallocate(mem_usage_step)
    deallocate(vm_size)
    deallocate(vm_size_step)

    write(*,*) 'Deallocated 2'

	call MPI_FINALIZE(mympi_ierr)

end program pes3dtest
