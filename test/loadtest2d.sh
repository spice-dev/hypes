#!/bin/bash

export OMP_NUM_THREADS=1

rm *.log

N_LIST=(1)

OUT_BASE=$1

for MPI_PROC in "${N_LIST[@]}"
do
	echo "Test for n=$DIM and n_cpu=$MPI_PROC is starting"
	TEST_NAME=test2d
	WORK_DIR=$OUT_BASE/$TEST_NAME/$MPI_PROC
	mkdir -p $WORK_DIR
	rm $WORK_DIR/$TEST_NAME.*.log
	CMD="mpirun -np $MPI_PROC -outfile-pattern=$WORK_DIR/$TEST_NAME.%r.log ../bin/autotest2d.bin -i in/umfpack-t.mat -o $WORK_DIR/$TEST_NAME-o.mat -b 1"
	echo $CMD
	eval $CMD

	echo "Test for n=$DIM and n_cpu=$MPI_PROC done"

	rm -f core.*
done

