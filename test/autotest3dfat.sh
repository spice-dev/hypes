
export OMP_NUM_THREADS=1

rm *.log

DIM_LIST=(512 768 1024)
N_LIST=(2 4 8 16 24 32)

BIN_FILE=$1
OUT_BASE=$2

for DIM in "${DIM_LIST[@]}"
do
	for MPI_PROC in "${N_LIST[@]}"
	do
		if [ "$DIM" -ge "$MPI_PROC" ]; then
 			echo "Test for n=$DIM and n_cpu=$MPI_PROC is starting"
			TEST_NAME=test-$DIM
			WORK_DIR=$OUT_BASE/$TEST_NAME/$MPI_PROC
			mkdir -p $WORK_DIR
			rm -f $WORK_DIR/$TEST_NAME.*.log
			rm -f $WORK_DIR/$TEST_NAME*.mat

			mpirun -np $MPI_PROC -outfile-pattern=$WORK_DIR/$TEST_NAME.%r.log ../bin/$BIN_FILE -o $WORK_DIR/$TEST_NAME-o.mat -x $DIM -y $DIM -z $DIM -a 2 -b 1

			echo "Test for n=$DIM and n_cpu=$MPI_PROC done"

			rm -f core.*
		else
			echo "$MPI_PROC processors is too much for $DIM x $DIM x $DIM"
		fi
	done
done
