# include all important stuff

include Makefile.inc


# simple clean routine
clean:
	rm -f ./src/*.mod
	rm -f ./src/*.o
	rm -f ./lib/*.a

# module for parsing command line arguments
getoptions:
	cd ./src/; \
	$(MPIF90) -c $(DEBUG_FLAGS) $(FC_FLAGS) getoptions.f90

# MATLAB file I/O for 2D solver
mio2d:
	cd ./src/; \
	$(MPIF90) -c $(FC_FLAGS) $(MATIO_INC) matrix_io2d.f90

# MATLAB file I/O for 3D solver
mio3d:
	cd ./src/; \
	$(MPIF90) -c $(FC_FLAGS) $(MATIO_INC) matrix_io3d.f90

# Sparse matrix tools for 2D solver
sm2dtools:
	cd ./src/; \
	$(MPIF90) -c $(RELEASE_FLAGS) $(FC_FLAGS) sm2d_tools.f90

# Sparse matrix tools for 3D solver
sm3dtools:
	cd ./src/; \
	$(MPIF90) -c $(RELEASE_FLAGS) $(FC_FLAGS) sm3d_tools.f90

# the same with debug options
sm2dtools-debug:
	cd ./src/; \
	$(MPIF90) -c $(DEBUG_FLAGS) $(FC_FLAGS) sm2d_tools.f90

sm3dtools-debug:
	cd ./src/; \
	$(MPIF90) -c $(DEBUG_FLAGS) $(FC_FLAGS) sm3d_tools.f90


# 3d solver routines
pes3d: sm3dtools
	cd ./src/; \
	$(MPIF90) -c $(RELEASE_FLAGS) $(FC_FLAGS) -I$(HYPRE_INCDIR) pes3d.f90

# 2D solver routines
pes2d: sm2dtools mio2d
	cd ./src/; \
	$(MPIF90) -c $(RELEASE_FLAGS) $(FC_FLAGS) $(MATIO_INC) -I$(HYPRE_INCDIR) pes2d.f90

# 3d solver routines
pes3d-debug: sm3dtools-debug
	cd ./src/; \
	$(MPIF90) -c $(DEBUG_FLAGS) $(FC_FLAGS) -I$(HYPRE_INCDIR) pes3d.f90

# 3d solver routines
pes2d-debug: sm2dtools-debug mio2d
	cd ./src/; \
	$(MPIF90) -c $(DEBUG_FLAGS) $(FC_FLAGS) -I$(HYPRE_INCDIR) pes2d.f90
# centralized solver module

lib: pes3d
	mkdir -p lib
	ar rcs ./lib/libhypes3d.a ./src/pes3d.o ./src/sm3d_tools.o

lib-debug: pes3d-debug
	mkdir -p lib
	ar rcs ./lib/libhypes3d-debug.a ./src/pes3d.o ./src/sm3d_tools.o
# centralized solver module

lib2d: pes2d
	mkdir -p lib
	ar rcs ./lib/libhypes2d.a ./src/pes2d.o ./src/sm2d_tools.o

lib2d-debug: pes2d-debug
	mkdir -p lib
	ar rcs ./lib/libhypes2d-debug.a ./src/pes2d.o ./src/sm2d_tools.o

# automatic testing programs
autotest: mio3d lib-debug getoptions
	mkdir -p bin; \
	cd ./src/; \
	$(MPIF90) $(DEBUG_FLAGS) $(FC_FLAGS_MKL) $(MATIO_INC)  autotest.f90 matrix_io3d.o getoptions.o ../lib/libhypes3d-debug.a -o ../bin/autotest$(SUFFIX_BIN) $(ALL_LIBS)

autotest2d: mio2d lib2d-debug getoptions
	mkdir -p bin; \
	cd ./src/; \
	$(MPIF90) $(DEBUG_FLAGS) $(FC_FLAGS_MKL) $(MATIO_INC)  autotest2d.f90 matrix_io2d.o getoptions.o ../lib/libhypes2d-debug.a -o ../bin/autotest2d$(SUFFIX_BIN) $(ALL_LIBS)

autotest-release: mio3d lib getoptions
	mkdir -p bin; \
	cd ./src/; \
	$(MPIF90) $(RELEASE_FLAGS) $(FC_FLAGS_MKL) $(MATIO_INC) autotest.f90 matrix_io3d.o getoptions.o ../lib/libhypes3d.a -o ../bin/autotest$(SUFFIX_BIN) $(ALL_LIBS)

autotest2d-release: mio2d lib2d getoptions
	mkdir -p bin; \
	cd ./src/; \
	$(MPIF90) $(RELEASE_FLAGS) $(FC_FLAGS_MKL) $(MATIO_INC) autotest2d.f90 matrix_io2d.o getoptions.o ../lib/libhypes2d.a -o ../bin/autotest2d$(SUFFIX_BIN) $(ALL_LIBS)

# just a library test
libtest: mio3d lib getoptions
	mkdir -p bin; \
	cd ./src/; \
	$(MPIF90) $(DEBUG_FLAGS) $(FC_FLAGS_MKL) $(MATIO_INC) autotest.f90 matrix_io3d.o getoptions.o ../lib/libhypes3d.a -o ../bin/libtest$(SUFFIX_BIN) $(ALL_LIBS)

libtest2d: mio2d lib2d getoptions
	mkdir -p bin; \
	cd ./src/; \
	$(MPIF90) $(DEBUG_FLAGS) $(FC_FLAGS_MKL) $(MATIO_INC) autotest2d.f90 matrix_io2d.o getoptions.o ../lib/libhypes2d.a -o ../bin/libtest2d$(SUFFIX_BIN) $(ALL_LIBS)

default: autotest-release autotest2d-release
	echo "Done"

install:
	mkdir -p bin
	mkdir -p lib
	mkdir -p $(PREFIX)/bin
	mkdir -p $(PREFIX)/include
	mkdir -p $(PREFIX)/lib
	cp -v bin/*test* $(PREFIX)/bin | true
	cp -v lib/*.a $(PREFIX)/lib | true
	cp -v src/*.mod $(PREFIX)/include | true
	cp -v src/*.h $(PREFIX)/include | true
