#!/usr/bin/env bash

## Print header
iterator=0;

while [ 1 ]; do

	line="";
	MILLIS=$(($(date +%s%N)/1000000))

    line=$(ps -C $1 -o vsize | tr '\n' ' ' | sed 's/VSZ//g')
	line="1 $line"
	if [ -n "$line" ]; then
        echo -e $line;
	fi

	let "iterator += 1";

	if [ $2 ]; then
		sleep $2;
	fi

done
