module pes3d

    use mpi
    use sm3d_tools

    implicit none

    include 'HYPREf.h'

    type solver_info
        ! these are pointers
        integer*8 :: hypre_solver
        integer*8 :: hypre_A
        integer*8 :: hypre_parcsr_A
        integer*8 :: hypre_b
        integer*8 :: hypre_x
        integer*8 :: hypre_par_b
        integer*8 :: hypre_par_x

        integer :: solver_error
        ! universal properties
        integer :: mat_dimension
        integer :: mat_nonzeros

        logical :: debug

        integer :: mpi_comm_solver
        integer :: mpi_rank
        integer*4 :: mpi_comm_size

        integer :: loc_row_start
        integer :: loc_row_end
        integer :: loc_col_start
        integer :: loc_col_end
        integer :: loc_size
        integer, dimension(:), pointer :: loc_rows

        integer, dimension(:,:), pointer :: partitioning

        integer :: n_parts = -1
        integer, dimension(:,:), pointer :: grid_partitioning

        real*8 :: n_0, d_x, d_y, d_z

    end type

contains


    subroutine pes3d_sync()

        implicit none

        integer :: ierr, mpirank, commsize, request, token, requestId, i
        integer, dimension(MPI_STATUS_SIZE) :: mpiStatus
        logical :: completed

        !AP This is a completely random number
        token = 24601
        requestId = 1701
        call MPI_COMM_SIZE(MPI_COMM_WORLD, commsize, ierr)
        call MPI_COMM_RANK(MPI_COMM_WORLD, mpirank, ierr)

        write (*,*) 'Syncing', mpirank, commsize


        if (mpirank == 0) then
            do i = 1, commsize - 1
                !write (*,*) 'Barrier sending', i, requestId
                call MPI_ISEND(token, 1, MPI_INTEGER, i, requestId, MPI_COMM_WORLD, request, ierr)
                call MPI_WAIT(request, mpiStatus, ierr)
            enddo
        else
            !write (*,*) 'Barrier waiting', requestId
            call MPI_IRECV(token, 1, MPI_INTEGER, 0, requestId, MPI_COMM_WORLD, request, ierr)

            completed = .false.

            do while (.not. completed)
                !write (*,*) 'testing waiting', requestId
                call MPI_TEST(request, completed, mpiStatus, ierr)
                call sleepqq(5);
                !write (*,*) 'waiting completed?', completed
            enddo
        endif

        write (*,*) 'Synced'
    end subroutine pes3d_sync

    subroutine pes3d_set_mpi(pes_info, MPI_COMM_SOLVER, n_parts, grid_partitioning)

        type (solver_info), intent(inout):: pes_info

        integer, intent(in) :: MPI_COMM_SOLVER

        integer, intent(in) :: n_parts
        integer, dimension(n_parts, 6), intent(in) :: grid_partitioning

        integer :: mpi_err
        integer :: curr_rank, part_size, p_start, x, y, z, n_x, n_y, n_z

        write (*,*) '*** PES *** Setting up MPI communicators and partitioning'

        pes_info%mpi_comm_solver = MPI_COMM_SOLVER

        call MPI_COMM_SIZE(pes_info%mpi_comm_solver, pes_info%mpi_comm_size, mpi_err)
        call MPI_COMM_RANK(pes_info%mpi_comm_solver, pes_info%mpi_rank, mpi_err)


        allocate(pes_info%grid_partitioning(n_parts, 6))
        pes_info%grid_partitioning(:,:) = grid_partitioning(:,:)
        pes_info%n_parts = n_parts

        allocate(pes_info%partitioning(5, 0:pes_info%mpi_comm_size - 1))

        n_x = maxval(pes_info%grid_partitioning(:,2))
        n_y = maxval(pes_info%grid_partitioning(:,4))
        n_z = maxval(pes_info%grid_partitioning(:,6))

        p_start = 1
        do curr_rank = 0, pes_info%n_parts - 1

            part_size = (pes_info%grid_partitioning(curr_rank + 1, 2) - pes_info%grid_partitioning(curr_rank + 1, 1) + 1) * &
             (pes_info%grid_partitioning(curr_rank + 1, 4) - pes_info%grid_partitioning(curr_rank + 1, 3) + 1) * &
             (pes_info%grid_partitioning(curr_rank + 1, 6) - pes_info%grid_partitioning(curr_rank + 1, 5) + 1);

            pes_info%partitioning(1, curr_rank) = p_start
            pes_info%partitioning(2, curr_rank) = p_start + part_size - 1
            p_start = p_start + part_size
            pes_info%partitioning(5, curr_rank) = part_size


            pes_info%partitioning(3, curr_rank) = 1
            pes_info%partitioning(4, curr_rank) = pes_info%mat_dimension
        enddo


    end subroutine pes3d_set_mpi

    subroutine pes3d_init(pes_info, MPI_COMM_SOLVER, debug, nX, nY, nZ, dX, dY, dZ, n_0, &
        n_x_loc, n_y_loc, n_z_loc, x_from_loc, y_from_loc, z_from_loc, flag_m_loc)
        implicit none

        type (solver_info), intent(inout):: pes_info

        integer, intent(in) :: MPI_COMM_SOLVER
        logical, intent(in) :: debug
        integer, intent(in) :: nX, nY, nZ
        real*8, intent(in) :: dX, dY, dZ, n_0
        integer, intent(in) :: n_x_loc, n_y_loc, n_z_loc, x_from_loc, y_from_loc, z_from_loc
        integer, dimension(n_x_loc, n_y_loc, n_z_loc), intent(inout) :: flag_m_loc

        real*8 :: strong_threshold

        if (pes_info%n_parts == -1) then
            write (*,*) 'MPI NOT INITIALIZED, ABORTING'
        endif

        pes_info%mat_dimension = nX * nY * nZ
        pes_info%debug = debug
        pes_info%d_x = dX
        pes_info%d_y = dY
        pes_info%d_z = dZ
        pes_info%n_0 = n_0
        strong_threshold = 0.55


        write (*,*) '*** PES *** Starting parallel Poison solver.'

        call pes3d_prepare_structures(pes_info)
        if (pes_info%debug) then
            write (*,*) '*** PES *** Defining the system'
        endif
        call pes3d_define(pes_info, nX, nY, nZ, dX, dY, dZ, n_x_loc, n_y_loc, n_z_loc, x_from_loc, y_from_loc, z_from_loc, flag_m_loc)
        if (pes_info%debug) then
            write (*,*) '*** PES *** HYPRE structures ready. Now starting solver.'
        endif

        ! everything is ready, prepare the solver now
        call HYPRE_BoomerAMGCreate(pes_info%hypre_solver, pes_info%solver_error)
        if (pes_info%debug) then
            write (*,*) '*** PES *** AMG solver created'
        endif

        !c        Set some parameters (See Reference Manual for more parameters)

        !c        print solve info + parameters = 3; nothing = 0
        if (pes_info%debug) then
            call HYPRE_BoomerAMGSetPrintLevel(pes_info%hypre_solver, 3, pes_info%solver_error)
            write (*,*) '*** PES *** HYPRE_BoomerAMGSetPrintLevel', pes_info%solver_error
        else
            call HYPRE_BoomerAMGSetPrintLevel(pes_info%hypre_solver, 0, pes_info%solver_error)
        endif
   !c        PMIS coarsening
        ! set 1
        !call HYPRE_BoomerAMGSetCoarsenType(pes_info%hypre_solver, 10, pes_info%solver_error)
        !call HYPRE_BoomerAMGSetInterpType(pes_info%hypre_solver, 6, pes_info%solver_error)
        !call HYPRE_BoomerAMGSetAggNumLevels(pes_info%hypre_solver, 4, pes_info%solver_error)

        ! set 2
!        call HYPRE_BoomerAMGSetCoarsenType(pes_info%hypre_solver, 3, pes_info%solver_error)
!         call HYPRE_BoomerAMGSetInterpType(pes_info%hypre_solver, 7, pes_info%solver_error)
!         call HYPRE_BoomerAMGSetAggNumLevels(pes_info%hypre_solver, 4, pes_info%solver_error)
!         call HYPRE_BoomerAMGSetCoarsenType(pes_info%hypre_solver, 10, pes_info%solver_error)
!         call HYPRE_BoomerAMGSetInterpType(pes_info%hypre_solver, 6, pes_info%solver_error)
!         call HYPRE_BoomerAMGSetAggNumLevels(pes_info%hypre_solver, 4, pes_info%solver_error)

        ! set 3
        ! call HYPRE_BoomerAMGSetNodal(pes_info%hypre_solver, 1, pes_info%solver_error)
        ! set 4
         !call HYPRE_BoomerAMGSetNodal(pes_info%hypre_solver, 3, pes_info%solver_error)
         !write (*,*) '*** PES *** HYPRE_BoomerAMGSetNodal 3', pes_info%solver_error

         !write (*,*) '*** PES *** HYPRE_BoomerAMGSetCoarsenType', pes_info%solver_error
!         !c        G-S/Jacobi hybrid relaxation
!         call HYPRE_BoomerAMGSetRelaxType(pes_info%hypre_solver, 4, pes_info%solver_error)
!         write (*,*) '*** PES *** HYPRE_BoomerAMGSetRelaxType 4', pes_info%solver_error
!         call HYPRE_BoomerAMGSetCycleType(pes_info%hypre_solver, 2, pes_info%solver_error)
!         write (*,*) '*** PES *** HYPRE_BoomerAMGSetCycleType 2', pes_info%solver_error

        !call HYPRE_BoomerAMGSetStrongThrshld(pes_info%hypre_solver, strong_threshold, pes_info%solver_error)
        ! write (*,*) '*** PES *** HYPRE_BoomerAMGSetStrengthThreshold', pes_info%solver_error

        !c        Sweeeps on each level
         !call HYPRE_BoomerAMGSetNumSweeps(pes_info%hypre_solver, 2, pes_info%solver_error)
         write (*,*) '*** PES *** HYPRE_BoomerAMGSetNumSweeps', pes_info%solver_error
!         !c         maximum number of levels
!         call HYPRE_BoomerAMGSetMaxLevels(pes_info%hypre_solver, 20, pes_info%solver_error)
!         write (*,*) '*** PES *** HYPRE_BoomerAMGSetMaxLevels', pes_info%solver_error
!         !c        conv. tolerance
!         call HYPRE_BoomerAMGSetTol(pes_info%hypre_solver, 1.0d-7, pes_info%solver_error)
!         write (*,*) '*** PES *** HYPRE_BoomerAMGSetTol', pes_info%solver_error

        if (pes_info%debug) then
            write (*,*) '*** PES *** AMG properties set', pes_info%solver_error
        endif

        !c        Now setup and solve!
        call HYPRE_BoomerAMGSetup(pes_info%hypre_solver, pes_info%hypre_parcsr_A, pes_info%hypre_par_b, pes_info%hypre_par_x, pes_info%solver_error)
        if (pes_info%debug) then
            write (*,*) '*** PES *** AMG setup done'
        endif
        call HYPRE_BoomerAMGSolve(pes_info%hypre_solver, pes_info%hypre_parcsr_A, pes_info%hypre_par_b, pes_info%hypre_par_x, pes_info%solver_error)

        if (pes_info%debug) then
            write (*,*) '*** PES *** AMG initial solve step done'
        endif

        !call HYPRE_IJVectorPrint(pes_info%hypre_x, "sol.dat", pes_info%solver_error)
        if (pes_info%debug) then
            write (*,*) '*** PES *** Init result printed'
        endif

        write (*,*) '*** PES *** 3D Poisson equation solver initialized.'
    end subroutine pes3d_init

    subroutine pes3d_define_partitioning(pes_info)
        implicit none

        type (solver_info), intent(inout):: pes_info



    end subroutine pes3d_define_partitioning

    subroutine pes3d_prepare_structures(pes_info)

        implicit none

        type (solver_info), intent(inout):: pes_info

        integer :: mpi_err, i, part_size
        real*8, dimension(:), allocatable :: all_zeros
        integer, dimension(:), allocatable :: all_rows

        if (pes_info%debug) then
            write (*,*) '*** PES *** HYPRE solver initialisation started'
        endif


        ! prepare matrix structure

        pes_info%loc_row_start = pes_info%partitioning(1, pes_info%mpi_rank)
        pes_info%loc_row_end = pes_info%partitioning(2, pes_info%mpi_rank)
        pes_info%loc_col_start = pes_info%partitioning(3, pes_info%mpi_rank)
        pes_info%loc_col_end = pes_info%partitioning(4, pes_info%mpi_rank)

        pes_info%loc_size = pes_info%partitioning(5, pes_info%mpi_rank)

        if (pes_info%debug) then
            write (*,*) '*** PES *** HYPRE solver partitioning defined'
            write (*,*) 'pes_info%loc_row_start', pes_info%loc_row_start
            write (*,*) 'pes_info%loc_row_end', pes_info%loc_row_end
            write (*,*) 'pes_info%loc_size', pes_info%loc_size
        endif

        allocate(pes_info%loc_rows(pes_info%loc_size))
        pes_info%loc_rows = 0

        do i = pes_info%loc_row_start, pes_info%loc_row_end
            pes_info%loc_rows(i - pes_info%loc_row_start + 1) = i
        enddo


        if (pes_info%debug) then
            write (*,*) '*** PES *** HYPRE matrix initialisation completed', pes_info%loc_row_start, pes_info%loc_row_end
            write (*,*) 'pes_info%loc_row_start, pes_info%loc_row_end', pes_info%loc_row_start, pes_info%loc_row_end
            write (*,*) 'pes_info%loc_col_start, pes_info%loc_col_end', pes_info%loc_col_start, pes_info%loc_col_end
            write (*,*) 'pes_info%loc_size', pes_info%loc_size
        endif

        call HYPRE_IJMatrixCreate(pes_info%mpi_comm_solver, pes_info%loc_row_start, pes_info%loc_row_end, pes_info%loc_row_start, pes_info%loc_row_end, pes_info%hypre_A, pes_info%solver_error)
        call HYPRE_IJMatrixSetObjectType(pes_info%hypre_A, HYPRE_PARCSR, pes_info%solver_error)
        call HYPRE_IJMatrixInitialize(pes_info%hypre_A, pes_info%solver_error)


        if (pes_info%debug) then
            write (*,*) '*** PES *** HYPRE matrix initialisation completed', pes_info%solver_error
            write (*,*) '*** PES *** MPI comm size:', pes_info%mpi_comm_size
            write (*,*) '*** PES *** MPI comm rank:', pes_info%mpi_rank
        endif

        ! matrix structure is ready, prepare vectors for rhs and solution

        if (pes_info%debug) then
            write (*,*) '*** PES *** Initializing HYPRE solution and rhs vectors:', pes_info%mpi_rank
        endif

        allocate(all_zeros(pes_info%loc_size))
        all_zeros = 0

        call HYPRE_IJVectorCreate(pes_info%mpi_comm_solver, pes_info%loc_row_start, pes_info%loc_row_end, pes_info%hypre_x, pes_info%solver_error)
        call HYPRE_IJVectorSetObjectType(pes_info%hypre_x, HYPRE_PARCSR, pes_info%solver_error)
        call HYPRE_IJVectorInitialize(pes_info%hypre_x, pes_info%solver_error)
        call HYPRE_IJVectorSetValues(pes_info%hypre_x, pes_info%loc_size, pes_info%loc_rows, all_zeros, pes_info%solver_error)
        call HYPRE_IJVectorAssemble(pes_info%hypre_x, pes_info%solver_error)
        call HYPRE_IJVectorGetObject(pes_info%hypre_x, pes_info%hypre_par_x, pes_info%solver_error)


        if (pes_info%debug) then
            !call HYPRE_IJVectorPrint(pes_info%hypre_x, "x.dat", pes_info%solver_error)

            write (*,*) '*** PES *** Solution initialized'
        endif

        if (pes_info%mpi_rank == 0) then
            all_zeros(1) = 1
        elseif (pes_info%mpi_rank + 1 == pes_info%mpi_comm_size) then
            all_zeros(pes_info%loc_size) = -1
        endif

        call HYPRE_IJVectorCreate(pes_info%mpi_comm_solver, pes_info%loc_row_start, pes_info%loc_row_end, pes_info%hypre_b, pes_info%solver_error)
        call HYPRE_IJVectorSetObjectType(pes_info%hypre_b, HYPRE_PARCSR, pes_info%solver_error)
        call HYPRE_IJVectorInitialize(pes_info%hypre_b, pes_info%solver_error)
        call HYPRE_IJVectorSetValues(pes_info%hypre_b, pes_info%loc_size, pes_info%loc_rows, all_zeros, pes_info%solver_error)
        call HYPRE_IJVectorAssemble(pes_info%hypre_b, pes_info%solver_error)
        call HYPRE_IJVectorGetObject(pes_info%hypre_b, pes_info%hypre_par_b, pes_info%solver_error)


        if (pes_info%debug) then
            !call HYPRE_IJVectorPrint(pes_info%hypre_b, "rhs.dat", pes_info%solver_error)
            write (*,*) '*** PES *** RHS initialized'
        endif

        deallocate(all_zeros)

        write (*,*) '*** PES *** All HYPRE structures are ready'



    end subroutine pes3d_prepare_structures

    subroutine pes3d_define(pes_info, n_x, n_y, n_z, d_x, d_y, d_z, n_x_loc, n_y_loc, n_z_loc, x_from_loc, y_from_loc, z_from_loc, flag_m_loc)
        implicit none
        type (solver_info), intent(inout):: pes_info

        integer, intent(in) :: n_x, n_y, n_z
        real*8, intent(in) :: d_x, d_y, d_z
        integer, intent(in) :: n_x_loc, n_y_loc, n_z_loc, x_from_loc, y_from_loc, z_from_loc
        integer, dimension(n_x_loc, n_y_loc, n_z_loc), intent(inout) :: flag_m_loc

        integer, dimension(1, n_x_loc * n_y_loc * n_z_loc) :: row_indices_loc
        integer, dimension(1, n_x_loc * n_y_loc * n_z_loc) :: nonzeros_loc
        integer, dimension(7, n_x_loc * n_y_loc * n_z_loc) :: col_indices_loc
        real*8, dimension(7, n_x_loc * n_y_loc * n_z_loc) :: values_loc
        integer :: total_nonzeros_loc, loc_row_from, loc_row_to, loc_col_from, loc_col_to

        integer :: i, curr_mpi_ierr, part_size
        character*2048 :: outputFile

        outputFile = 'matrix.mat'


        if (pes_info%debug) then
            write(*,*) 'Grid dimensions xyz', n_x, n_y, n_z
            write(*,*) 'Loc dimensions xyz', n_x_loc, n_y_loc, n_z_loc
        endif

        call sm3d_get_parmat(n_x, n_y, n_z, n_x_loc, n_y_loc, n_z_loc, x_from_loc, y_from_loc, z_from_loc, d_x, d_y, d_z, pes_info%n_parts, pes_info%grid_partitioning, &
        flag_m_loc, row_indices_loc, nonzeros_loc, col_indices_loc, values_loc, total_nonzeros_loc, loc_row_from, loc_row_to, loc_col_from, loc_col_to)


        if (pes_info%debug) then
            !write(*,*) 'row_indices_loc', row_indices_loc
            !write(*,*) 'col_indices_loc', col_indices_loc
            !write(*,*) 'values_loc', values_loc
            write(*,*) 'total_nonzeros_loc', total_nonzeros_loc, pes_info%loc_size
        endif

        write(*,*) pes_info%loc_row_start, pes_info%loc_row_end
        do i = 1, pes_info%loc_size
            !write(*,*) 'loc mat', i, col_indices_loc(1:7, i)
            call HYPRE_IJMatrixSetValues(pes_info%hypre_A, 1, nonzeros_loc(1, i), pes_info%loc_row_start + i - 1, col_indices_loc(1:7, i), values_loc(1:7, i), pes_info%solver_error)
        enddo

        call HYPRE_IJMatrixGetObject(pes_info%hypre_A, pes_info%hypre_parcsr_A, pes_info%solver_error)
        call HYPRE_IJMatrixAssemble(pes_info%hypre_A, pes_info%solver_error)

        if (pes_info%debug) then
            call HYPRE_IJMatrixPrint(pes_info%hypre_A, "matrix.dat", pes_info%solver_error)
            write (*,*) 'Matrix written.'
        endif

    end subroutine pes3d_define




    subroutine pes3d_solve(pes_info, n_x_loc, n_y_loc, n_z_loc, flag_m_loc, pot_m_loc, dens_m_loc, newpot_m_loc)
        implicit none
        ! solver
        type (solver_info), intent(inout):: pes_info

        ! input properties
        integer, intent(in) :: n_x_loc, n_y_loc, n_z_loc

        integer, dimension(n_x_loc, n_y_loc, n_z_loc), intent(in):: flag_m_loc
        real*8, dimension(n_x_loc, n_y_loc, n_z_loc), intent(in):: pot_m_loc, dens_m_loc
        real*8, dimension(n_x_loc, n_y_loc, n_z_loc), intent(out):: newpot_m_loc


        real*8, dimension(pes_info%loc_size) :: vector_loc

        integer :: i
        integer*8 :: timeStart, timeEnd, timeDiff

        !newpot_m_loc = 0
        !write (*,*) '*** PES *** AMG solve step'
        timeStart = 0
        timeEnd = 0
        timeDiff = 0


        if (pes_info%debug) then
            call gettime(timeStart)
        endif

        call sm3d_rhs_par(n_x_loc, n_y_loc, n_z_loc, pes_info%n_0, pes_info%d_x, pes_info%d_y, pes_info%d_z, flag_m_loc, pot_m_loc, dens_m_loc, vector_loc)


        if (pes_info%debug) then
            call gettime(timeEnd)
            timeDiff = timeEnd - timeStart
            write (*,*) '#time_diff;rhs;', timeDiff
            call gettime(timeStart)
        endif


        call HYPRE_IJVectorInitialize(pes_info%hypre_b, pes_info%solver_error)
        call HYPRE_IJVectorSetValues(pes_info%hypre_b, pes_info%loc_size, pes_info%loc_rows, vector_loc, pes_info%solver_error)
        call HYPRE_IJVectorAssemble(pes_info%hypre_b, pes_info%solver_error)
        call HYPRE_IJVectorGetObject(pes_info%hypre_b, pes_info%hypre_par_b, pes_info%solver_error)


        call HYPRE_BoomerAMGSolve(pes_info%hypre_solver, pes_info%hypre_parcsr_A, pes_info%hypre_par_b, pes_info%hypre_par_x, pes_info%solver_error)

        if (pes_info%debug) then
            call HYPRE_IJVectorPrint(pes_info%hypre_b, "rhs1.dat", pes_info%solver_error)
            call HYPRE_IJVectorPrint(pes_info%hypre_x, "sol1.dat", pes_info%solver_error)
            write (*,*) '*** PES *** AMG solve step done'
        endif

        !

        call HYPRE_IJVectorGetValues(pes_info%hypre_x, pes_info%loc_size, pes_info%loc_rows, vector_loc, pes_info%solver_error)

        if (pes_info%debug) then
            call gettime(timeEnd)
            timeDiff = timeEnd - timeStart
            write (*,*) '#time_diff;hypresolve;', timeDiff

            call gettime(timeStart)
        endif
        call sm3d_result_par(n_x_loc, n_y_loc, n_z_loc, vector_loc, newpot_m_loc)

        if (pes_info%debug) then
            call gettime(timeEnd)
            timeDiff = timeEnd - timeStart
            write (*,*) '#time_diff;reshaping;', timeDiff
        endif
        !call exit(0)
    end subroutine pes3d_solve

    subroutine pes3d_stop(pes_info)
        implicit none

        ! solver
        type (solver_info), intent(inout):: pes_info


        deallocate(pes_info%partitioning)
        deallocate(pes_info%loc_rows)
        deallocate(pes_info%grid_partitioning)

        call HYPRE_BoomerAMGDestroy(pes_info%hypre_solver, pes_info%solver_error)
        call HYPRE_IJMatrixDestroy(pes_info%hypre_A, pes_info%solver_error)
        call HYPRE_IJVectorDestroy(pes_info%hypre_b, pes_info%solver_error)
        call HYPRE_IJVectorDestroy(pes_info%hypre_x, pes_info%solver_error)

        write (*,*) '*** PES *** Finalized.'

    end subroutine pes3d_stop


    subroutine pes3d_gather_i(pes_info, n_x, n_y, n_z, int_mat_glob, n_x_loc, n_y_loc, n_z_loc, int_mat_loc)
        implicit none
        ! solver
        type (solver_info), intent(inout):: pes_info
        integer, intent(in) :: n_x, n_y, n_z
        integer, dimension(:,:,:), intent(inout) :: int_mat_glob
        integer, intent(in) :: n_x_loc, n_y_loc, n_z_loc
        integer, dimension(n_x_loc, n_y_loc, n_z_loc), intent(in) :: int_mat_loc

        integer :: x_from, x_to, y_from, y_to, z_from, z_to, n_loc, i_r

        integer :: mympi_ierr
        integer :: mympi_status(MPI_STATUS_SIZE)

        call MPI_BARRIER(pes_info%mpi_comm_solver, mympi_ierr)

        do i_r = 0, pes_info%mpi_comm_size - 1
            x_from = pes_info%grid_partitioning(i_r + 1, 1)
            x_to = pes_info%grid_partitioning(i_r + 1, 2)
            y_from = pes_info%grid_partitioning(i_r + 1, 3)
            y_to = pes_info%grid_partitioning(i_r + 1, 4)
            z_from = pes_info%grid_partitioning(i_r + 1, 5)
            z_to = pes_info%grid_partitioning(i_r + 1, 6)


            if (i_r .eq. 0) then
                if (pes_info%mpi_rank .eq. 0) then
                    int_mat_glob(x_from:x_to,y_from:y_to,z_from:z_to) = int_mat_loc
                endif
            else
                n_loc = (x_to-x_from+1)*(y_to-y_from+1)*(z_to-z_from+1)
                if (pes_info%mpi_rank .eq. 0) then
                    call mpi_recv(int_mat_glob(x_from:x_to,y_from:y_to,z_from:z_to), n_loc, MPI_DOUBLE, i_r, 299000 + i_r, pes_info%mpi_comm_solver, mympi_status, mympi_ierr)
                elseif (pes_info%mpi_rank .eq. i_r) then
                    call mpi_send(int_mat_loc, n_loc, MPI_DOUBLE, 0, 299000 + i_r, pes_info%mpi_comm_solver, mympi_ierr)
                endif
            endif

        enddo
    end subroutine pes3d_gather_i

    subroutine pes3d_gather_r(pes_info, n_x, n_y, n_z, real_mat_glob, n_x_loc, n_y_loc, n_z_loc, real_mat_loc)
        implicit none
        ! solver
        type (solver_info), intent(inout):: pes_info
        integer, intent(in) :: n_x, n_y, n_z
        real*8, dimension(:,:,:), intent(inout) :: real_mat_glob
        integer, intent(in) :: n_x_loc, n_y_loc, n_z_loc
        real*8, dimension(n_x_loc, n_y_loc, n_z_loc), intent(in) :: real_mat_loc

        integer :: x_from, x_to, y_from, y_to, z_from, z_to, n_loc, i_r

        integer :: mympi_ierr
        integer :: mympi_status(MPI_STATUS_SIZE)

        call MPI_BARRIER(pes_info%mpi_comm_solver, mympi_ierr)

        do i_r = 0, pes_info%mpi_comm_size - 1
            x_from = pes_info%grid_partitioning(i_r + 1, 1)
            x_to = pes_info%grid_partitioning(i_r + 1, 2)
            y_from = pes_info%grid_partitioning(i_r + 1, 3)
            y_to = pes_info%grid_partitioning(i_r + 1, 4)
            z_from = pes_info%grid_partitioning(i_r + 1, 5)
            z_to = pes_info%grid_partitioning(i_r + 1, 6)


            if (i_r .eq. 0) then
                if (pes_info%mpi_rank .eq. 0) then
                    real_mat_glob(x_from:x_to,y_from:y_to,z_from:z_to) = real_mat_loc
                endif
            else
                n_loc = (x_to-x_from+1)*(y_to-y_from+1)*(z_to-z_from+1)
                if (pes_info%mpi_rank .eq. 0) then
                    call mpi_recv(real_mat_glob(x_from:x_to,y_from:y_to,z_from:z_to), n_loc, MPI_DOUBLE, i_r, 298000 + i_r, pes_info%mpi_comm_solver, mympi_status, mympi_ierr)
                elseif (pes_info%mpi_rank .eq. i_r) then
                    call mpi_send(real_mat_loc, n_loc, MPI_DOUBLE, 0, 298000 + i_r, pes_info%mpi_comm_solver, mympi_ierr)
                endif
            endif

        enddo
    end subroutine pes3d_gather_r

    subroutine pes3d_scatter_i(pes_info, n_x, n_y, n_z, int_mat_glob, n_x_loc, n_y_loc, n_z_loc, int_mat_loc)
        implicit none
        ! solver
        type (solver_info), intent(inout):: pes_info
        integer, intent(in) :: n_x, n_y, n_z
        integer, dimension(:, :, :), intent(in) :: int_mat_glob
        integer, intent(in) :: n_x_loc, n_y_loc, n_z_loc
        integer, dimension(n_x_loc, n_y_loc, n_z_loc), intent(out) :: int_mat_loc

        integer :: x_from, x_to, y_from, y_to, z_from, z_to, n_loc, i_r

        integer :: mympi_ierr
        integer :: mympi_status(MPI_STATUS_SIZE)

        i_r = 0

        call MPI_BARRIER(pes_info%mpi_comm_solver, mympi_ierr)

        !write(*,*) 'pes_info%mpi_comm_size  - 1', pes_info%mpi_comm_size  - 1
        do i_r = 0, pes_info%mpi_comm_size  - 1
            write(*,*), i_r
            x_from = pes_info%grid_partitioning(i_r + 1, 1)
            x_to = pes_info%grid_partitioning(i_r + 1, 2)
            y_from = pes_info%grid_partitioning(i_r + 1, 3)
            y_to = pes_info%grid_partitioning(i_r + 1, 4)
            z_from = pes_info%grid_partitioning(i_r + 1, 5)
            z_to = pes_info%grid_partitioning(i_r + 1, 6)

            if (i_r .eq. 0) then
                if (pes_info%mpi_rank .eq. 0) then
                    int_mat_loc = int_mat_glob(x_from:x_to,y_from:y_to,z_from:z_to)
                endif
            else
                n_loc = (x_to-x_from+1)*(y_to-y_from+1)*(z_to-z_from+1)
                !write(*,*) 'n_loc', n_loc
                if (pes_info%mpi_rank .eq. 0) then
                    call mpi_send(int_mat_glob(x_from:x_to,y_from:y_to,z_from:z_to), n_loc, MPI_INT, i_r, 993000 + i_r, pes_info%mpi_comm_solver, mympi_ierr)
                elseif (pes_info%mpi_rank .eq. i_r) then
                    call mpi_recv(int_mat_loc, n_loc, MPI_INT, 0, 993000 + i_r, pes_info%mpi_comm_solver, mympi_status, mympi_ierr)
                endif
            endif
        enddo
    end subroutine pes3d_scatter_i

    subroutine pes3d_scatter_r(pes_info, n_x, n_y, n_z, real_mat_glob, n_x_loc, n_y_loc, n_z_loc, real_mat_loc)
        implicit none
        ! solver
        type (solver_info), intent(inout):: pes_info
        integer, intent(in) :: n_x, n_y, n_z
        real*8, dimension(:, :, :), intent(in) :: real_mat_glob
        integer, intent(in) :: n_x_loc, n_y_loc, n_z_loc
        real*8, dimension(n_x_loc, n_y_loc, n_z_loc), intent(out) :: real_mat_loc

        integer :: x_from, x_to, y_from, y_to, z_from, z_to, n_loc, i_r

        integer :: mympi_ierr
        integer :: mympi_status(MPI_STATUS_SIZE)

        call MPI_BARRIER(pes_info%mpi_comm_solver, mympi_ierr)

        do i_r = 0, pes_info%mpi_comm_size  - 1
!            write(*,*), i_r
            x_from = pes_info%grid_partitioning(i_r + 1, 1)
            x_to = pes_info%grid_partitioning(i_r + 1, 2)
            y_from = pes_info%grid_partitioning(i_r + 1, 3)
            y_to = pes_info%grid_partitioning(i_r + 1, 4)
            z_from = pes_info%grid_partitioning(i_r + 1, 5)
            z_to = pes_info%grid_partitioning(i_r + 1, 6)

            if (i_r .eq. 0) then
                if (pes_info%mpi_rank .eq. 0) then
                    real_mat_loc = real_mat_glob(x_from:x_to,y_from:y_to,z_from:z_to)
                endif
            else
                n_loc = (x_to-x_from+1)*(y_to-y_from+1)*(z_to-z_from+1)
                !write(*,*) 'n_loc', n_loc
                if (pes_info%mpi_rank .eq. 0) then
                    call mpi_send(real_mat_glob(x_from:x_to,y_from:y_to,z_from:z_to), n_loc, MPI_DOUBLE, i_r, 992000 + i_r, pes_info%mpi_comm_solver, mympi_ierr)
                elseif (pes_info%mpi_rank .eq. i_r) then
                    call mpi_recv(real_mat_loc, n_loc, MPI_DOUBLE, 0, 992000 + i_r, pes_info%mpi_comm_solver, mympi_status, mympi_ierr)
                endif
            endif
        enddo
    end subroutine pes3d_scatter_r



end module pes3d
