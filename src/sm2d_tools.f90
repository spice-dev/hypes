module sm2d_tools

    use mpi

    implicit none

contains

    subroutine sm2d_get_parmat(n_y, n_z, n_y_loc, n_z_loc, y_from_loc, z_from_loc, d_y, d_z, n_parts, grid_partitioning, &
        flag_m_loc, row_indices_loc, non_zeros_loc, col_indices_loc, values_loc, total_nonzeros_loc, loc_row_from, loc_row_to, loc_col_from, loc_col_to)

        implicit none

        integer, intent(in) :: n_y, n_z, n_y_loc, n_z_loc, y_from_loc, z_from_loc
        real*8, intent(in) :: d_y, d_z
        integer, intent(in) :: n_parts
        integer, dimension(n_parts, 6), intent(in) :: grid_partitioning
        integer, dimension(n_y_loc, n_z_loc), intent(in) :: flag_m_loc

        integer, dimension(1, n_y_loc * n_z_loc), intent(out) :: row_indices_loc
        integer, dimension(1, n_y_loc * n_z_loc), intent(out) :: non_zeros_loc
        integer, dimension(5, n_y_loc * n_z_loc), intent(out) :: col_indices_loc
        real*8, dimension(5, n_y_loc * n_z_loc), intent(out) :: values_loc
        integer, intent(out) :: total_nonzeros_loc

        integer, intent(out) :: loc_row_from, loc_row_to, loc_col_from, loc_col_to

        integer :: y, z, flag, i, left, right, top, bottom, center
        real*8 :: qy, qz, ry, rz, ryz

        row_indices_loc = 0
        non_zeros_loc = 0
        col_indices_loc = 0
        values_loc = 0

        qy = 1 / (d_y * d_y)
        qz = 1 / (d_z * d_z)
        ry = - 2 / (d_y * d_y)
        rz = - 2 / (d_z * d_z)
        ryz = ry + rz

        write (*,*) 'Matrix elements', qy, qz, ryz, qz, qy

        i = 1
        !write(*,*) 'x_from_loc, to', x_from_loc, x_from_loc + n_x_loc - 1
        !write(*,*) 'y_from_loc, to', y_from_loc, y_from_loc + n_y_loc - 1
        !write(*,*) 'z_from_loc, to', z_from_loc, z_from_loc + n_z_loc - 1

        do y = y_from_loc, y_from_loc + n_y_loc - 1
            do z = z_from_loc, z_from_loc + n_z_loc - 1
                flag = flag_m_loc(y - y_from_loc + 1, z - z_from_loc + 1)

                center = sm2d_linearIndex_par(n_y, n_z, y, z, n_parts, grid_partitioning)

                row_indices_loc(1, i) = center

                if (flag == 0) then
                !if (.true.) then

                    col_indices_loc(1:5, i) = (/center, 0, 0, 0, 0/)
                    values_loc(1:5, i) = (/1.0, 0.0, 0.0, 0.0, 0.0/)
                    non_zeros_loc(1, i) = 1

                else

                    bottom = sm2d_linearIndex_par(n_y, n_z, y, z - 1, n_parts, grid_partitioning)
                    top = sm2d_linearIndex_par(n_y, n_z, y, z + 1, n_parts, grid_partitioning)
                    left = sm2d_linearIndex_par(n_y, n_z, y - 1, z, n_parts, grid_partitioning)
                    right = sm2d_linearIndex_par(n_y, n_z, y + 1, z, n_parts, grid_partitioning)

                    col_indices_loc(1:5, i) = (/top, left, center, right, bottom/)
                    values_loc(1:5, i) = (/qz, qy, ryz, qy, qz/)
                    non_zeros_loc(1, i) = 5
                endif

                !write(*,*) x, y, z
                !write(*,*) center, '>', col_indices_loc(1:7, i)
                i = i + 1
            enddo
        enddo

        loc_row_from = minval(row_indices_loc)
        loc_row_to = maxval(row_indices_loc)
        loc_col_from = minval(col_indices_loc)
        loc_col_to = maxval(col_indices_loc)

        total_nonzeros_loc = sum(non_zeros_loc)
    end subroutine

    subroutine sm2d_getStencil(n_y, n_z, d_y, d_z, flagMatrix, row_indices, non_zeros, col_indices, values, total_non_zeros)

        implicit none

        integer, intent(in) :: n_y, n_z
        real*8, intent(in) :: d_y, d_z
        integer, dimension(n_y, n_z), intent(in) :: flagMatrix

        integer, dimension(1, n_y * n_z), intent(out) :: row_indices
        integer, dimension(1, n_y * n_z), intent(out) :: non_zeros
        integer, dimension(5, n_y * n_z), intent(out) :: col_indices
        real*8, dimension(5, n_y * n_z), intent(out) :: values
        integer, intent(out) :: total_non_zeros

        integer :: y, z, flag, i, left, right, top, bottom, center
        real*8 :: qy, qz, ry, rz, ryz

        row_indices = 0
        non_zeros = 0
        col_indices = 0
        values = 0

        qy = +1 / (d_y * d_y)
        qz = +1 / (d_z * d_z)
        ry = - 2 / (d_y * d_y)
        rz = - 2 / (d_z * d_z)
        ryz = ry + rz

        i = 1
        do y = 1, n_y
            do z = 1, n_z
                flag = flagMatrix(y, z)

                center = sm2d_linearIndex(n_y, n_z, y, z)

                row_indices(1, i) = center

                if (flag == 0) then

                    col_indices(1:5, i) = (/center, 0, 0, 0, 0/)
                    values(1:5, i) = (/1.0, 0.0, 0.0, 0.0, 0.0/)
                    non_zeros(1, i) = 1


                else

                    bottom = sm2d_linearIndex(n_y, n_z, y, z - 1)
                    top = sm2d_linearIndex(n_y, n_z, y, z + 1)
                    left = sm2d_linearIndex(n_y, n_z, y - 1, z)
                    right = sm2d_linearIndex(n_y, n_z, y + 1, z)

                    col_indices(1:5, i) = (/top, left, center, right, bottom/)
                    values(1:5, i) = (/qz, qy, ryz, qy, qz/)
                    non_zeros(1, i) = 5


                endif
            i = i + 1
            enddo
        enddo

        total_non_zeros = sum(non_zeros)

    end subroutine sm2d_getStencil

    subroutine sm2d_rhs(n_y, n_z, n_0, d_y, d_z, flagMatrix, potentialMatrix, densityMatrix, rightHandSide)

        implicit none

        ! input properties
        integer, intent(in) :: n_y, n_z
        real*8, intent(in) :: n_0, d_z, d_y

        integer, dimension(n_y, n_z), intent(in):: flagMatrix
        real*8, dimension(n_y, n_z), intent(in):: potentialMatrix, densityMatrix
        real*8, dimension(n_y * n_z), intent(out) :: rightHandSide

        integer :: y, z, matrixIndex
        real*8 :: charge, chargeQ

        integer*8 :: timeStart, timeEnd, timeDiff

        rightHandSide = 0
        matrixIndex = 1

        !write(*,*) 'sm_rhs', n_z, n_y, d_z, d_y, n0, (real(n0) * d_z * d_y)

        do y = 1, n_y
            do z = 1, n_z
                ! 1 = object; 0 = plasma
                charge = - densityMatrix(y, z) / (n_0 * d_z * d_y)

                rightHandSide(matrixIndex) = merge(potentialMatrix(y, z), charge, flagMatrix(y, z) .eq. 0)
                !write(*,*) rightHandSide(matrixIndex)

                matrixIndex = matrixIndex + 1
            enddo
        enddo

    end subroutine sm2d_rhs

    subroutine sm2d_rhs_par(n_y_loc, n_z_loc, n_0, d_y, d_z, flag_m_loc, pot_m_loc, dens_m_loc, rhs)

        implicit none

        ! input properties
        integer, intent(in) :: n_y_loc, n_z_loc
        real*8, intent(in) :: n_0, d_y, d_z

        integer, dimension(n_y_loc, n_z_loc), intent(in):: flag_m_loc
        real*8, dimension(n_y_loc, n_z_loc), intent(in):: pot_m_loc, dens_m_loc
        real*8, dimension(n_y_loc * n_z_loc), intent(out) :: rhs

        integer :: y, z, matrixIndex
        real*8 :: charge, chargeQ

        integer*8 :: timeStart, timeEnd, timeDiff

        rhs = 0
        matrixIndex = 1

        !write(*,*) 'sm_rhs', n_z, n_y, d_z, d_y, n0, (real(n0) * d_z * d_y)


        do y = 1, n_y_loc
            do z = 1, n_z_loc
                ! 1 = object; 0 = plasma
                charge = - dens_m_loc(y, z) / (n_0 * d_z * d_y)

                rhs(matrixIndex) = merge(pot_m_loc(y, z), charge, flag_m_loc(y, z) .eq. 0)
                !write(*,*) rhs(matrixIndex)

                matrixIndex = matrixIndex + 1
            enddo
        enddo

    end subroutine sm2d_rhs_par

    integer function sm2d_linearIndex_par(n_y, n_z, y, z, n_parts, grid_partitioning)

        implicit none
        integer, intent(in) :: n_y, n_z, y, z, n_parts
        integer, dimension(n_parts, 6) :: grid_partitioning
        integer :: n_y_loc, n_z_loc, i, li_offset, y_loc, z_loc, y1, z1

        y1 = mod(y + n_y, n_y)
        z1 = mod(z + n_z, n_z)

        if (y1 == 0) y1 = n_y
        if (z1 == 0) z1 = n_z

        sm2d_linearIndex_par = 0
        do i = 1, n_parts
            n_y_loc = grid_partitioning(i, 4) - grid_partitioning(i, 3) + 1;
            n_z_loc = grid_partitioning(i, 6) - grid_partitioning(i, 5) + 1;
            if (y1 .ge. grid_partitioning(i, 3) .and. y1 .le. grid_partitioning(i, 4) .and. &
                z1 .ge. grid_partitioning(i, 5) .and. z1 .le. grid_partitioning(i, 6)) then
                y_loc = y1 - grid_partitioning(i, 3) + 1
                z_loc = z1 - grid_partitioning(i, 5) + 1
                sm2d_linearIndex_par = sm2d_linearIndex_par + sm2d_linearIndex(n_y_loc, n_z_loc, y_loc, z_loc)
                return
            else
                sm2d_linearIndex_par = sm2d_linearIndex_par + n_y_loc * n_z_loc;
            endif

        enddo

        !write(*,*) sm2d_linearIndex_par
        return

    end function sm2d_linearIndex_par

    integer function sm2d_linearIndex(n_y, n_z, y, z)

        implicit none
        integer, intent(in) :: n_y, n_z, y, z
        integer :: y1, z1

        y1 = mod(y + n_y, n_y)
        z1 = mod(z + n_z, n_z)

        if (y1 == 0) y1 = n_y
        if (z1 == 0) z1 = n_z

        sm2d_linearIndex = n_z * (y1 - 1) + z1

        return

    end function sm2d_linearIndex

    subroutine sm2d_result(elementCount, rhs, n_y, n_z, newPotMatrix)
        implicit none

        integer, intent(in) :: elementCount
        real*8, dimension(elementCount), intent(in) :: rhs
        integer, intent(in) :: n_y, n_z
        real*8, dimension(n_y, n_z), intent(inout) :: newPotMatrix

        integer ::  y, z, matrixIndex

        matrixIndex = 1
        do y = 1, n_y
            do z = 1, n_z
                newPotMatrix(y, z) = rhs(matrixIndex)
                !write(*,*) rhs(matrixIndex)
                matrixIndex = matrixIndex + 1
            enddo
        enddo

    end subroutine sm2d_result

    subroutine sm2d_result_par(n_y_loc, n_z_loc, rhs, newPotMatrix)
        implicit none

        real*8, dimension(n_y_loc*n_z_loc), intent(in) :: rhs
        integer, intent(in) :: n_y_loc, n_z_loc
        real*8, dimension(n_y_loc, n_z_loc), intent(inout) :: newPotMatrix

        integer :: y, z, matrixIndex

        matrixIndex = 1
        do y = 1, n_y_loc
            do z = 1, n_z_loc
                newPotMatrix(y, z) = real(rhs(matrixIndex))
                !write(*,*) rhs(matrixIndex)
                matrixIndex = matrixIndex + 1
            enddo
        enddo

    end subroutine sm2d_result_par
end module

